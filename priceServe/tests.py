from django.test import TestCase
from .datarelated.helpers import parserAndClean
from .datarelated import testData

import json


class TestParserAndClean(TestCase):
    def setUp(self):
        self.entry1, self.entry2, self.entry3 = json.loads(testData.testData)[
            0:3]
        self.entry4 = json.loads(testData.testData2)[2]

    def test_bad_data1(self):
        """price is an empty string"""

        out = parserAndClean(self.entry1)

        assert out == {'02cd22d2e02b4a7086641ab9df01b': {'in_stock': False, 'id': '02cd22d2e02b4a7086641ab9df01b',
                                                         'brand': 'typothere', 'price': None, 'retailer': 'caffiso', 'name': 'superplausible'}}

    def test_bad_data2(self):
        """in_stock is 'no' and null price"""

        out = parserAndClean(self.entry2)

        assert out == {'e026f8a429754008ac9b2c6c0b': {'retailer': 'remail', 'brand': 'badan',
                                                      'price': None, 'name': 'kallege', 'in_stock': False, 'id': 'e026f8a429754008ac9b2c6c0b'}}

    def test_bad_data3(self):
        """missing brand data"""

        out = parserAndClean(self.entry3)

        assert out == {'149136f7ff3c4de89': {'retailer': 'sokemanemot', 'brand': None,
                                             'price': 680.32, 'name': 'isochlor', 'in_stock': False, 'id': '149136f7ff3c4de89'}}

    def test_bad_data4(self):
        """negative price"""

        out = parserAndClean(self.entry4)
        assert out == {'149136f7ff3c4de89': {'id': '149136f7ff3c4de89', 'retailer': 'sokemanemot',
                                             'in_stock': False, 'price': None, 'brand': 'juncture', 'name': 'isochlor'}}
