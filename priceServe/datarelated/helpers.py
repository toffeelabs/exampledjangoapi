import json


def validPrice(x):
    if x is None:
        return False
    if x is "":
        return False
    try:
        float(x)
    except ValueError:
        return False
    if float(x) < 0:
        return False
    return True


def parserAndClean(entry, **kwargs):

    goodInStockResponses = ["y", "yes", "yy"]
    goodNotInStockResponses = ["n", "no", "nn", "not", "0"]
    strLabels = ["id", "name", "brand", "retailer"]

    tmpdatabase = {}
    itemID = entry['id']

    tmpdatabase[itemID] = {}

    if entry.get('in_stock') in goodInStockResponses:
        tmpdatabase[itemID]["in_stock"] = True
    elif entry.get('in_stock') in goodNotInStockResponses:
        tmpdatabase[itemID]["in_stock"] = False
    else:
        tmpdatabase[itemID]["in_stock"] = None

    if validPrice(entry.get('price')) is False:
        tmpdatabase[itemID]['price'] = None
    else:
        tmpdatabase[itemID]['price'] = float(entry['price'])

    for label in strLabels:
        if entry.get(label) is not None:
            tmpdatabase[itemID][label] = entry[label]
        else:
            tmpdatabase[itemID][label] = None

    return tmpdatabase
