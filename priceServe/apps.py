from django.apps import AppConfig

import urllib.request
import csv
import json
import logging

from .datarelated.helpers import parserAndClean
from priceServe.datarelated.database import DBClass
from pricesearcher.settings import CUSTOM_PRODUCT_SOURCE_SETTINGS

class PriceserveConfig(AppConfig):
	name = 'priceServe'
	verbose_name = "Simple item price server api"

	logging.basicConfig(format='%(asctime)s %(message)s',level=logging.INFO)

	def ready(self):
		"""Takes a local csv file and downloads a remote .json file listed in
		CUSTOM_PRODUCT_SOURCE_SETTINGS in /pricesearcher/settings.py

		It then parses and cleans the entries, and merges them.
		Currently saves to a remote dict in DBClass (no getter/setter implemented)
		"""

		logging.info(" ~~~ Basic data cleaner and django api. By Lloyd Ashby ~~~ ")
		logging.info("Building in memory product database")
		itemsUrl = CUSTOM_PRODUCT_SOURCE_SETTINGS['remote']['url']
		path = CUSTOM_PRODUCT_SOURCE_SETTINGS['local']['path']
		database = {}
		try:
			with open(path, newline='') as csvfile:
				spamreader = csv.DictReader(csvfile, fieldnames=[
					'id', 'name', 'brand', 'retailer', 'price', 'in_stock'], skipinitialspace=True)
				sniff = csv.Sniffer()
				has_header = sniff.has_header(csvfile.read(2048))
				csvfile.seek(0)
				logging.info("Processing csv file: %s", path)
				for row in spamreader:
					if has_header: #skip the first line if it has headers.
						has_header = False
						continue
					itemID = row['id']
					if itemID is None:
						continue

					if itemID in database:  # Skip on duplicates
						continue
					try:
						cleanItem = parserAndClean(row)
					except Exception as e:
						logging.error("Error reading entry in csv file")
						continue
					database.update(cleanItem)

		except Exception as e:
			logging.error("FILE ERROR: %s", e)

		logging.info("Downloading from: %s", itemsUrl)

		try:
			with urllib.request.urlopen(itemsUrl) as url:
				remoteData = json.loads(url.read().decode())

			logging.info("Processing download")
			for entry in remoteData:
				itemID = entry['id']
				if itemID is None:
					continue

				try:
					cleanItem = parserAndClean(entry)
				except Exception as e:
					logging.error("Error reading entry in remote json")
					continue
				database.update(cleanItem)

		except Exception as e:
			logging.error("Error. could not download and merge")

		DBClass.database = database
		logging.info("Total entries in database: " + str(len(database)))
