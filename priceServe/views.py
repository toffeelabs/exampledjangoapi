from django.shortcuts import render
from django.http import HttpResponse, JsonResponse
import json
import logging
from cerberus import Validator
from priceServe.datarelated.database import DBClass


def index(request):
    """The default landing page"""
    return HttpResponse("Simple products api. Visit domain.com/api/get/[PRODUCT_ID] and get a JSON formated response.")


def default(request):
    """The default api response when no valid queries are submitted"""
    exampleDatastr = '{"errorMsg": "No item id specified. Example schema.", "status": "ok", "result": {"in_stock": false, "retailer": "example Retailer Name", "name": "Product Name", "brand": "Example Brand Name", "id": "1234abc", "price": 42.0}}'
    exampleData = json.loads(exampleDatastr)
    logging.info("Sending back payload: ", exampleData)
    return JsonResponse(exampleData, safe=True, json_dumps_params=None)


def list_all(request):
    """List all available product IDs (heavy)"""
    database = DBClass.database
    keys = database.keys()
    jsonResult = {'status': 'error', 'result': {}, 'errorMsg': None}
    if len(keys) is 0:
        return HttpResponse(status=500)
    jsonResult['status'] = "ok"
    jsonResult['result'].update({'ids': list(keys)})
    logging.info("Sending back payload containing: %s product ids", len(keys))

    return JsonResponse(jsonResult, safe=True, json_dumps_params=None)


def get(request, getId):
    """ The most basic of get view. The requested product id is taken from the url and searched for in the DB (Currently just an in memory db)
        The entry is inserted into our results json structure and the shema is checked. We also output some basic bookeeping info: ('status' and 'errorMsg')
    """
    if request.method == 'GET':
        database = DBClass.database
        labels = ["id", "name", "brand", "retailer", "price", "in_stock"]

        # id is a string
        # name is a string
        # brand is a string
        # retailer is a string
        # price is a float
        # in_stock is a boolean

        jsonResult = {'status': 'error', 'result': {}, 'errorMsg': None}
        if getId not in database:
            jsonResult['errorMsg'] = "id not found in database"
            return JsonResponse(jsonResult, safe=True, json_dumps_params=None)

        out = database[getId]
        jsonResult['status'] = "ok"

        for label in labels:
            if label in out.keys():
                jsonResult["result"][label] = out[label]
            else:
                jsonResult["result"][label] = None

        logging.info("Sending back payload: %s", jsonResult)

        schema = {'price': {'nullable': True, 'type': 'float', 'min': 0},
                  'id': {'type': 'string'},
                  'retailer': {'nullable': True, 'type': 'string'},
                  'brand': {'nullable': True, 'type': 'string'},
                  'name': {'nullable': True, 'type': 'string'},
                  'in_stock': {'type': 'boolean'}

                  }
        v = Validator()

        if v.validate(jsonResult['result'], schema) is False:
            return HttpResponse(status=500)

        return JsonResponse(jsonResult, safe=True, json_dumps_params=None)
    else:
        return HttpResponse(status=404)
