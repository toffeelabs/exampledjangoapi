from django.urls import path

from . import views

urlpatterns = [
    path('', views.index, name='index'),
    path('api/', views.default, name='default'),
    path('api/list/', views.list_all, name='list_all'),
    path('api/get/', views.default, name='default'),
    path('api/get/<str:getId>', views.get, name='get'),
]
