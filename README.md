# Example Django Api product directory

A quick example of a django project that imports data from a csv and a remote json and serves it up via a simple no thrills view.
No DB.
Not a model view controller pattern
Some tests.
run `make setup` inside a new python3 virtual env followed by `make run`

When using manage.py you MUST run this with `python manage.py runserver --noreload` (or a wsgi server) otherwise the data import code will run twice.

And as is traditional with all systems that accept arbitrary data from the internet:
>"It worked just _fine_ on _my_ machine! O_O"

![NO WARRANTY!](https://media.giphy.com/media/rftarkt7Ki2Gs/giphy.gif)
