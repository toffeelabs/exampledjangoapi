setup:
	pip install -r requirements.txt

run:
	python manage.py runserver --noreload

test:
	find . -name '*.pyc' -delete
	python manage.py test
